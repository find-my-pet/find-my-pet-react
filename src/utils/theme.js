import { createMuiTheme } from '@material-ui/core/styles'
import indigo from '@material-ui/core/colors/indigo'
import teal from '@material-ui/core/colors/teal'
import blueGrey from '@material-ui/core/colors/blueGrey'

export const theme = createMuiTheme({
  palette: {
    primary: indigo,
    secondary: teal,
    blueGrey,
    backgroundBlue: 'rgb(240, 240, 247)',
  },
  elevation: `0px 1px 5px 0px rgba(0, 0, 0, 0.2),
    0px 2px 2px 0px rgba(0, 0, 0, 0.14),
    0px 3px 1px -2px rgba(0, 0, 0, 0.12)`,
  typography: {
    useNextVariants: true,
  },
})
