export const simpleStates = {
  user: null,
  userInfo: null,
  menuEsquerdoAberto: false,
  listaDialog: [],
  urlState: {},
  loadingUser: true,
  isOpenModalRules: false,
  snackbarMessages: [],
}
