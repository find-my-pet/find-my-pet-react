import { createStore as createStoreRedux, applyMiddleware, compose } from 'redux'
import { responsiveStoreEnhancer } from 'redux-responsive'

export const createStore = rootReducer =>
  createStoreRedux(
    rootReducer,
    compose(
      responsiveStoreEnhancer,
      applyMiddleware(),
    ),
  )
