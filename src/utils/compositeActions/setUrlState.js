import { store } from 'utils/store'

import { dispatchToStore } from './dispatchToStore'

export const setUrlState = novasPropriedades => {
  const urlStateAtual = store.getState().urlState
  const novoUrlState = { ...urlStateAtual, ...novasPropriedades }
  dispatchToStore('select', 'urlState', novoUrlState)
}
