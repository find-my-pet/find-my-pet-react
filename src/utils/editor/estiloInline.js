import React from 'react'

import { FormatBold, FormatItalic, FormatUnderlined } from '@material-ui/icons'

export const ESTILO_INLINE = [
  { icon: <FormatBold />, style: 'BOLD' },
  { icon: <FormatItalic />, style: 'ITALIC' },
  { icon: <FormatUnderlined />, style: 'UNDERLINE' },
]
