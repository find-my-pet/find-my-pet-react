import React from 'react'

import { FormatQuote, Code, FormatListBulleted, FormatListNumbered } from '@material-ui/icons'

export const ESTILO_BLOCK = [
  { icon: <FormatListBulleted />, style: 'unordered-list-item' },
  { icon: <FormatListNumbered />, style: 'ordered-list-item' },
  { icon: <FormatQuote />, style: 'blockquote' },
  { icon: <Code />, style: 'code-block' },
]
