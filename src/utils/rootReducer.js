import { combineReducers } from 'redux'
import { createResponsiveStateReducer } from 'redux-responsive'

import { createSimpleReducers } from 'local-modules/simple-redux'
import { createStringsReducer } from 'local-modules/strings'
import { languages } from 'utils/strings'
import { simpleStates } from './simpleStates'

export const rootReducer = combineReducers({
  browser: createResponsiveStateReducer({
    extraSmall: 600,
    small: 960,
    medium: 1280,
    large: 1920,
    extraLarge: 9000,
  }),
  strings: createStringsReducer(languages),
  ...createSimpleReducers(simpleStates),
  // dialogConfigArray: reducer('dialogConfigArray'),
  // barraSuperiorOptionsConfig: reducer('barraSuperiorOptionsConfig')
})
