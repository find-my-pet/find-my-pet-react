import { createSimpleActions } from 'local-modules/simple-redux'
import { simpleStates } from 'utils/simpleStates'

export const actions = createSimpleActions(simpleStates)
