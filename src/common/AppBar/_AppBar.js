import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'

import BackIcon from '@material-ui/icons/ArrowBack'

import { Container } from 'common/Container'

import { UserButton } from './UserButton'

export class _AppBar extends Component {
  static propTypes = propTypes

  render() {
    const { classes, title, goBack, hideUserButton } = this.props
    return (
      <Paper className={classes.root}>
        <Container>
          <div className={classes.items}>
            <div className={classes.title}>
              {goBack && (
                <IconButton className={classes.icon} onClick={goBack}>
                  <BackIcon />
                </IconButton>
              )}
              <Typography className={classes.titleText} variant="h6">
                {title}
              </Typography>
            </div>
            {!hideUserButton && <UserButton />}
          </div>
        </Container>
      </Paper>
    )
  }
}
