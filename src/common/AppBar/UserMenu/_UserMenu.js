import React, { Component } from 'react'
import { propTypes } from './proptypes'

import MenuItem from '@material-ui/core/MenuItem'
import MenuList from '@material-ui/core/MenuList'
import { Divider } from '@material-ui/core'

import { logout } from 'api/logout'

export class _UserMenu extends Component {
  static propTypes = propTypes

  logout = () => {
    this.props
      .closeMenu()
      .then(() => setTimeout(logout, 500))
      .catch(e => console.error(e))
  }

  render() {
    const { user } = this.props
    return (
      <MenuList>
        <MenuItem disabled>{user.email}</MenuItem>
        <Divider />
        <MenuItem onClick={this.logout}>Sair</MenuItem>
      </MenuList>
    )
  }
}
