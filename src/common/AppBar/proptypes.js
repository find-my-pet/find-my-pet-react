import PropTypes from 'prop-types'

export const propTypes = {
  title: PropTypes.string.isRequired,
  goBack: PropTypes.func,
  hideUserButton: PropTypes.bool,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}
