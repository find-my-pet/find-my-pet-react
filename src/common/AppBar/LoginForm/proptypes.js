import PropTypes from 'prop-types'

export const propTypes = {
  closeMenu: PropTypes.func.isRequired,
  // redux actions
  pushSnackbar: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
}
