import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'

import { SimpleForm, CampoText } from 'local-modules/simple-form'
import { setUrlState } from 'local-modules/url-state'
import { login } from 'api/login'

export class _LoginForm extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      value: {
        email: '',
        password: '',
      },
      isValid: false,
    }
  }

  onChangeValue = value => this.setState({ value })

  onValid = isValid => this.setState({ isValid })

  handleSignIn = () => {
    this.login()
      .then(this.loginSuccess)
      .catch(this.showSignInError)
  }

  showSignInError = e => {
    const { pushSnackbar } = this.props
    pushSnackbar(e.message)
    console.error(e)
  }

  login = () => {
    const { value } = this.state
    return login(value.email, value.password)
  }

  loginSuccess = () => {
    const { pushSnackbar, strings, closeMenu } = this.props
    closeMenu()
    pushSnackbar(strings.signInSuccess)
  }

  handleSignUp = () => {
    this.props.closeMenu()
    setUrlState({ hidden: { isSignUpActive: true } })
  }

  render() {
    const { classes, strings } = this.props
    const { value } = this.state
    return (
      <Paper className={classes.formPaper}>
        <SimpleForm onChange={this.onChangeValue} onValid={this.onValid} valor={value}>
          <CampoText
            accessor="email"
            required
            textFieldProps={{
              label: strings.email,
              fullWidth: true,
              variant: 'outlined',
            }}
          />
          <CampoText
            accessor="password"
            required
            textFieldProps={{
              label: strings.password,
              fullWidth: true,
              type: 'password',
              variant: 'outlined',
            }}
          />
        </SimpleForm>
        <Button onClick={this.handleSignIn} className={classes.button} color="secondary" variant="contained">
          {strings.signIn}
        </Button>
        <Button onClick={this.handleSignUp} className={classes.button}>
          {strings.signUp}
        </Button>
        <Typography className={classes.forgot} variant="caption">
          {strings.forgotPassword}
        </Typography>
      </Paper>
    )
  }
}
