export const style = theme => ({
  formPaper: {
    padding: '10px',
  },
  forgot: {
    margin: '10px 0 0 0',
    '&:hover': {
      color: theme.palette.secondary.main,
    },
  },
  button: {
    margin: '0',
  },
})
