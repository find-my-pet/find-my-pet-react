import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
// import { withRouter } from 'react-router'

// import { withStrings } from 'local-modules/strings'
import { _LoginForm } from './_LoginForm'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const LoginForm = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(_LoginForm)
