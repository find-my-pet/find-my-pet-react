export const style = theme => ({
  root: {
    position: 'sticky',
    bottom: '9999999999px',
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
  },
  items: {
    minHeight: 48,
    display: 'flex',
    alignItems: 'center',
    padding: '0 10px',
    justifyContent: 'space-between',
  },
  title: {
    display: 'flex',
    alignItems: 'center',
  },
  titleText: {
    color: theme.palette.common.white,
  },
  icon: {
    color: theme.palette.common.white,
  },
})
