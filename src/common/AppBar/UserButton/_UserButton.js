import React, { Component } from 'react'
import { propTypes } from './proptypes'

import IconButton from '@material-ui/core/IconButton'
import Popover from '@material-ui/core/Popover'

import AccountCircleIcon from '@material-ui/icons/AccountCircle'

import { LoginForm } from '../LoginForm'
import { UserMenu } from '../UserMenu'

export class _UserButton extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      anchorEl: null,
    }
  }

  handleClick = event => {
    this.setState({
      anchorEl: event.currentTarget,
    })
  }

  handleClose = () => {
    return new Promise(resolve =>
      this.setState(
        {
          anchorEl: null,
        },
        resolve,
      ),
    )
  }

  render() {
    const { classes, user } = this.props
    const { anchorEl } = this.state
    const open = Boolean(anchorEl)
    return (
      <div>
        {!user && (
          <IconButton className={classes.icon} onClick={this.handleClick}>
            <AccountCircleIcon />
          </IconButton>
        )}
        {user && (
          <IconButton className={classes.icon} onClick={this.handleClick}>
            <AccountCircleIcon color="secondary" />
          </IconButton>
        )}
        <Popover
          id="simple-popper"
          open={open}
          anchorEl={anchorEl}
          onClose={this.handleClose}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'center',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'center',
          }}
        >
          {!user && <LoginForm closeMenu={this.handleClose} />}
          {user && <UserMenu closeMenu={this.handleClose} />}
        </Popover>
      </div>
    )
  }
}
