import PropTypes from 'prop-types'

export const propTypes = {
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
}
