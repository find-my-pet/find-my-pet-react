export const style = theme => {
  return {
    root: {
      position: 'sticky',
      bottom: '0px',
      backgroundColor: theme.palette.blueGrey[50],
    },
    menu: {
      display: 'flex',
    },
    menuItem: {
      flexGrow: 1,
    },
  }
}
