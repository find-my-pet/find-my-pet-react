import { compose } from 'redux'
// import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router'

import { withStrings } from 'local-modules/strings'
import { _MenuBottom } from './_MenuBottom'
import { style } from './style'
// import { mapStateToProps, mapDispatchToProps } from './redux'

export const MenuBottom = compose(
  withStyles(style),
  withStrings,
  // connect(
  //   mapStateToProps,
  //   mapDispatchToProps,
  // ),
  withRouter,
)(_MenuBottom)
