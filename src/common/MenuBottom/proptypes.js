import PropTypes from 'prop-types'

const compositionTypes = PropTypes.oneOf(['search', 'lost', 'donation', 'searchWithDialog']).isRequired

export const propTypes = {
  composition: PropTypes.arrayOf(compositionTypes).isRequired,
  handleOpenFilter: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
}
