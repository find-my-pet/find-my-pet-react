import PropTypes from 'prop-types'

export const propTypes = {
  variant: PropTypes.oneOf(['lost', 'donation', 'search', 'searchWithDialog']).isRequired,
  handleOpenFilter: PropTypes.func,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}
