const menuItem = theme => ({
  backgroundColor: theme.palette.common.white,
  width: '50%',
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  '&:hover': {
    backgroundColor: theme.palette.secondary.main,
    '& $searchContent': {
      color: theme.palette.common.black,
    },
  },
})

export const style = theme => ({
  menuItem: menuItem(theme),
  menuItemSearch: {
    ...menuItem(theme),
    backgroundColor: theme.palette.primary.main,
  },
  searchContent: {
    color: theme.palette.common.white,
    '& :hover': {
      color: theme.palette.common.black,
    },
  },
  // menuItemDefault: {
  //   backgroundColor: theme.palette.common.white,
  //   '& :hover': {
  //     color: theme.palette.common.black,
  //   },
  // },
  search: {
    display: 'flex',
  },
})
