import React, { Component } from 'react'
import { propTypes } from './proptypes'

import MenuItem from '@material-ui/core/MenuItem'
import Typography from '@material-ui/core/Typography'
import ListItemIcon from '@material-ui/core/ListItemIcon'

import SearchIcon from '@material-ui/icons/Search'

import { setUrlState } from 'local-modules/url-state'

export class _MenuBottomItem extends Component {
  static propTypes = propTypes

  goToLost = () => {
    setUrlState({ pathname: '/', hidden: {} })
  }

  goToDonation = () => {
    setUrlState({ pathname: '/donation', hidden: {} })
  }

  render() {
    const { variant, strings, classes, handleOpenFilter } = this.props
    switch (variant) {
      case 'lost':
        return (
          <MenuItem className={classes.menuItem} onClick={this.goToLost}>
            <Typography variant="subtitle1">{strings.lost}</Typography>
          </MenuItem>
        )
      case 'donation':
        return (
          <MenuItem className={classes.menuItem} onClick={this.goToDonation}>
            <Typography variant="subtitle1">{strings.donation}</Typography>
          </MenuItem>
        )
      case 'search':
        return (
          <MenuItem onClick={handleOpenFilter} className={classes.menuItemSearch}>
            <ListItemIcon>
              <SearchIcon className={classes.searchContent} />
            </ListItemIcon>
            <Typography className={classes.searchContent} variant="subtitle1">
              {strings.search}
            </Typography>
          </MenuItem>
        )
      case 'searchWithDialog':
        return (
          <MenuItem onClick={handleOpenFilter} className={classes.menuItemSearch}>
            <ListItemIcon>
              <SearchIcon className={classes.searchContent} />
            </ListItemIcon>
            <Typography className={classes.searchContent} variant="subtitle1">
              {strings.search}
            </Typography>
          </MenuItem>
        )
      default:
        console.error('MenuBottom unidentified variant: ' + variant)
        return null
    }
  }
}
