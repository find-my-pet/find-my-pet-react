import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Paper from '@material-ui/core/Paper'

import { Container } from 'common/Container'

import { MenuBottomItem } from './MenuBottomItem'

export class _MenuBottom extends Component {
  static propTypes = propTypes

  render() {
    const { classes, composition, handleOpenFilter } = this.props
    return (
      <Paper className={classes.root}>
        <Container>
          <div className={classes.menu}>
            {composition.map(variant => {
              return <MenuBottomItem key={variant} variant={variant} handleOpenFilter={handleOpenFilter} />
            })}
          </div>
        </Container>
      </Paper>
    )
  }
}
