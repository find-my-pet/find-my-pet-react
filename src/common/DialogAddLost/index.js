import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
import { withRouter } from 'react-router'

import { _DialogAddLost } from './_DialogAddLost'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const DialogAddLost = compose(
  withStyles(style),
  withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(_DialogAddLost)
