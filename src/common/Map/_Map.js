import React, { Component } from 'react'
import { propTypes } from './proptypes'

import { googleKey } from 'api/googleKey'

export class _Map extends Component {
  static propTypes = propTypes

  render() {
    const { classes } = this.props
    return (
      <iframe
        className={classes.map}
        title="map"
        width="100%"
        height="450"
        frameBorder="0"
        src={`https://www.google.com/maps/embed/v1/place?key=${googleKey}
&q=Space+Needle,Seattle+WA`}
        allowFullScreen
      />
    )
  }
}
