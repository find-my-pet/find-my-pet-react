import PropTypes from 'prop-types'

export const propTypes = {
  loading: PropTypes.bool,
  text: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
  // match: PropTypes.object.isRequired,
}
