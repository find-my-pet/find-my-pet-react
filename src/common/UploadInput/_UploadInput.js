import React, { Component } from 'react'
import { propTypes } from './proptypes'

import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'

import { UploadButton } from 'common/UploadButton'

export class _UploadInput extends Component {
  static propTypes = propTypes

  render() {
    const { loading, text, onChange, classes } = this.props
    return (
      <div className={classes.container}>
        {loading && <CircularProgress className={classes.text} />}
        {!loading && <Typography className={classes.text}>{text}</Typography>}
        <UploadButton onChange={onChange} />
      </div>
    )
  }
}
