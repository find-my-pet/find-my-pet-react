export const style = theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
  },
  text: {
    margin: '0 10px 0 0',
  },
})
