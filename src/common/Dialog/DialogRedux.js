import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function dialogMapStateToProps(state) {
  return {
    listaDialog: state.listaDialog,
  }
}

export function dialogMapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      listaDialogShift: actions.shift.listaDialog,
    },
    dispatch,
  )
}
