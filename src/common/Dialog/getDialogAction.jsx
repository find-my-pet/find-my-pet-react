import React from 'react'
import Button from '@material-ui/core/Button'

export const getDialogAction = closeDialog => (actionConfig, index) => {
  const { onClick, autoFocus } = actionConfig
  const onClickPersonalizado = () => {
    if (onClick) onClick()
    closeDialog()
  }
  return (
    <Button key={index} onClick={onClickPersonalizado} color="primary" autoFocus={autoFocus || false}>
      {actionConfig.texto}
    </Button>
  )
}
