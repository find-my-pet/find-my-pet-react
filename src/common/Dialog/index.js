import { DialogComponent } from './DialogComponent'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'

import { dialogMapStateToProps, dialogMapDispatchToProps } from './DialogRedux'
import { dialogStyle } from './dialogStyle'

export const Dialog = compose(
  withStyles(dialogStyle),
  connect(
    dialogMapStateToProps,
    dialogMapDispatchToProps,
  ),
)(DialogComponent)
