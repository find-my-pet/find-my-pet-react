import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Button from '@material-ui/core/Button'

export class _UploadButton extends Component {
  static propTypes = propTypes

  render() {
    const { classes, onChange, className } = this.props
    return (
      <div className={className}>
        <input accept="image/*" className={classes.input} id="raised-button-file" type="file" onChange={onChange} />
        <label htmlFor="raised-button-file">
          <Button variant="contained" component="span" className={classes.button}>
            Upload
          </Button>
        </label>
      </div>
    )
  }
}
