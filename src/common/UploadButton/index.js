import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
// import { withRouter } from 'react-router'

import { _UploadButton } from './_UploadButton'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const UploadButton = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(_UploadButton)
