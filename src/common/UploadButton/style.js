export const style = theme => ({
  button: {
    margin: '0 0 10px 0',
  },
  input: {
    display: 'none',
  },
})
