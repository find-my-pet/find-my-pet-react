import PropTypes from 'prop-types'

export const propTypes = {
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
  // match: PropTypes.object.isRequired,
}
