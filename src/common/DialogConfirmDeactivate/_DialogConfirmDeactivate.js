import React, { Component } from 'react'
import { propTypes } from './proptypes'

import { Dialog, Slide, Paper, Typography, Button } from '@material-ui/core'

import { updateUrlState } from 'local-modules/url-state'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export class _DialogConfirmDeactivate extends Component {
  static propTypes = propTypes

  close = () => {
    updateUrlState({ hidden: { isDialogConfirmDeactivateActive: false, instanceId: null } })
  }

  deactivateInstance = () => {
    const { collection, location } = this.props
    const { instanceId } = location.state.hidden
    collection
      .doc(instanceId)
      .update({ active: false })
      .then(this.close)
      .catch(this.showError)
  }

  showError = e => {
    const { pushSnackbar } = this.props
    pushSnackbar(e.message)
    console.error(e)
    this.close()
  }

  render() {
    const { history, text, strings, classes } = this.props
    const { isDialogConfirmDeactivateActive } = history.location.state.hidden
    return (
      <Dialog open={Boolean(isDialogConfirmDeactivateActive)} onClose={this.close} TransitionComponent={Transition}>
        <Paper>
          <Typography variant="h6" className={classes.text}>
            {strings.confirm}
          </Typography>
          <Typography className={classes.text}>{text}</Typography>
          <Button className={classes.button} variant="contained" color="secondary" onClick={this.deactivateInstance}>
            {strings.confirm}
          </Button>
          <Button className={classes.button} onClick={this.close}>
            {strings.cancel}
          </Button>
        </Paper>
      </Dialog>
    )
  }
}
