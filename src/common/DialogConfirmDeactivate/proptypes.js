import PropTypes from 'prop-types'

export const propTypes = {
  text: PropTypes.string.isRequired,
  collection: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
  // redux actions
  pushSnackbar: PropTypes.func.isRequired,
}
