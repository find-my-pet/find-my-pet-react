export const style = {
  containerInner: {
    padding: '10px',
  },
  button: {
    margin: '0 0 0 0',
  },
  map: { margin: '0 0 10px 0' },
}
