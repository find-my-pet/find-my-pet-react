import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'

import { SimpleForm, CampoText } from 'local-modules/simple-form'
import { updateUrlState } from 'local-modules/url-state'

import { AppBar } from 'common/AppBar'
import { Container } from 'common/Container'
import { signUp } from 'api/signUp'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export class _SignUpForm extends Component {
  static propTypes = propTypes

  state = {
    value: {
      email: '',
      password: '',
    },
    isValid: false,
  }

  onValid = isValid => this.setState({ isValid })

  onChangeValue = value => this.setState({ value })

  close = () => {
    updateUrlState({ hidden: { isSignUpActive: false } })
  }

  signUp = () => {
    const { value } = this.state
    signUp(value.email, value.password, { isOrg: false, contactInformation: '' })
      .then(this.signUpSuccess)
      .catch(this.signUpFail)
  }

  signUpSuccess = () => {
    const { pushSnackbar, strings } = this.props
    this.setState({ value: { email: '', password: '' } })
    this.close()
    pushSnackbar(strings.signUpSuccess)
  }

  signUpFail = e => {
    const { pushSnackbar } = this.props
    pushSnackbar(e.message)
  }

  render() {
    const { classes, strings, history } = this.props
    const { value } = this.state
    const { isSignUpActive } = history.location.state.hidden
    return (
      <Dialog fullScreen open={Boolean(isSignUpActive)} onClose={this.handleClose} TransitionComponent={Transition}>
        <div className={classes.root}>
          <AppBar hideUserButton goBack={this.close} title={strings.signUp} />
          <div className={classes.content}>
            <Container>
              <div className={classes.containerInner}>
                <SimpleForm onChange={this.onChangeValue} onValid={this.onValid} valor={value}>
                  <CampoText
                    accessor="email"
                    required
                    textFieldProps={{
                      label: strings.email,
                      fullWidth: true,
                      variant: 'outlined',
                    }}
                  />
                  <CampoText
                    accessor="password"
                    required
                    textFieldProps={{
                      label: strings.password,
                      type: 'password',
                      fullWidth: true,
                      variant: 'outlined',
                    }}
                  />
                </SimpleForm>
                <Button variant="contained" color="secondary" className={classes.button} onClick={this.signUp}>
                  {strings.signUp}
                </Button>
              </div>
            </Container>
          </div>
        </div>
      </Dialog>
    )
  }
}
