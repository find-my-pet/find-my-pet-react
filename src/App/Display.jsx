import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import { Container } from 'common/Container'

const COR_FUNDO = 'rgb(240, 240, 247)'

const displayStyle = theme => ({
  display: {
    backgroundColor: COR_FUNDO,
    '& > div': {
      [theme.breakpoints.up('md')]: {},
    },
    '& header': {
      padding: '0 !important',
    },
    width: '100%',
    height: '100%',
  },
})

const DisplayComponent = props => {
  const { classes, children } = props
  return (
    <div className={classes.display}>
      <Container>{children}</Container>
    </div>
  )
}

DisplayComponent.propTypes = {
  children: PropTypes.element,
  // style
  classes: PropTypes.object.isRequired,
  // browser: PropTypes.object.isRequired
}

export const Display = withStyles(displayStyle)(DisplayComponent)
