import React, { Component } from 'react'
import { propTypes } from './proptypes'
import { SimpleForm, CampoText } from 'local-modules/simple-form'

export class _AddForm extends Component {
  static propTypes = propTypes

  render() {
    const { strings, onChange, onValid, valor } = this.props
    return (
      <SimpleForm onChange={onChange} onValid={onValid} valor={valor}>
        <CampoText
          accessor="description"
          required
          textFieldProps={{
            label: strings.description,
            fullWidth: true,
            multiline: true,
            rows: 2,
            rowsMax: 10,
            variant: 'outlined',
          }}
        />
        <CampoText
          accessor="contactInformation"
          required
          textFieldProps={{
            label: strings.contactInformation,
            fullWidth: true,
            multiline: true,
            rows: 2,
            rowsMax: 10,
            variant: 'outlined',
          }}
        />
      </SimpleForm>
    )
  }
}
