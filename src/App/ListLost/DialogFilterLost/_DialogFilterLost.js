import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import Paper from '@material-ui/core/Paper'

import { updateUrlState } from 'local-modules/url-state'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export class _DialogFilterLost extends Component {
  static propTypes = propTypes

  close = () => {
    updateUrlState({ hidden: { isDialogFilterActive: false } })
  }

  openFilter = () => {
    updateUrlState({ hidden: { isFilterActive: true, isDialogFilterActive: false } })
  }

  render() {
    const { classes, strings, history } = this.props
    const { isDialogFilterActive } = history.location.state.hidden
    return (
      <Dialog open={Boolean(isDialogFilterActive)} onClose={this.close} TransitionComponent={Transition}>
        <Paper>
          <MenuList>
            <MenuItem className={classes.item} onClick={this.openFilter}>
              {strings.dialogLostFilter}
            </MenuItem>
            <MenuItem className={classes.item} onClick={this.openFilter}>
              {strings.dialogFoundFilter}
            </MenuItem>
          </MenuList>
        </Paper>
      </Dialog>
    )
  }
}
