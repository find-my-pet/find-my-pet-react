import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
// import { withRouter } from 'react-router'

import { _ListLost } from './_ListLost'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const ListLost = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(_ListLost)
