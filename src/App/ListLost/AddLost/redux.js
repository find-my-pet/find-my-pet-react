import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    user: state.user,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      pushSnackbar: actions.push.snackbarMessages,
    },
    dispatch,
  )
}
