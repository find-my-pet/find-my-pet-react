import React, { Component } from 'react'
import uid from 'uuid/v4'

import { propTypes } from './proptypes'
import { getInitialState } from './getInitialState'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'
import Typography from '@material-ui/core/Typography'

import { updateUrlState } from 'local-modules/url-state'

import { AppBar } from 'common/AppBar'
import { Container } from 'common/Container'
import { UploadInput } from 'common/UploadInput'
import { Map } from 'common/Map'

import { lostCollection } from 'api/collections'
import { upload } from 'api/upload'
import { getDownloadLink } from 'api/getDownloadLink'

import { AddForm } from '../AddForm'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export class _AddLost extends Component {
  static propTypes = propTypes

  state = getInitialState()

  close = () => updateUrlState({ hidden: { isAddActive: false } })

  onValid = isValid => this.setState({ isValid })

  onChangeValue = value => this.setState({ value })

  save = () => {
    const { user } = this.props
    const { isValid, image, loadingImage, value, imageId, imageUrl } = this.state
    const canSave = isValid && image !== '' && !loadingImage
    if (canSave) {
      const { contactInformation, description } = value
      const instanceData = {
        imageId,
        imageUrl,
        contactInformation,
        description,
        userId: user ? user.uid : null,
        active: true,
      }
      lostCollection
        .add(instanceData)
        .then(this.afterSave)
        .catch(r => {
          console.error(r)
          this.afterSave()
        })
    } else console.error('Value not valid.')
  }

  afterSave = () => {
    this.setState(this.props.initialState)
    this.close()
  }

  onChangeImage = e => {
    const file = e.target.files[0] ? e.target.files[0] : null
    const imageName = file ? file.name : 'Adicionar foto'
    if (file) {
      this.setState({ loadingImage: true })
      const imageId = uid()
      upload(imageId, file)
        .then(() => getDownloadLink(imageId))
        .then(this.setImageState({ file, imageName, imageId }))
        .catch(this.uploadImageFail)
    } else this.setState({ image: '', imageName, imageId: null })
  }

  setImageState = ({ file, imageName, imageId }) => imageUrl => {
    this.setState({ imageUrl, image: file, imageName, imageId, loadingImage: false })
  }

  uploadImageFail = e => {
    const { pushSnackbar, strings } = this.props
    this.setState({ loadingImage: false })
    console.error(e)
    const message = e.message ? e.message : strings.uploadImageFail
    pushSnackbar(message)
  }

  render() {
    const { classes, strings, history } = this.props
    const { value, imageName, loadingImage } = this.state
    const { isAddActive } = history.location.state.hidden
    return (
      <Dialog fullScreen open={Boolean(isAddActive)} onClose={this.handleClose} TransitionComponent={Transition}>
        <div className={classes.root}>
          <AppBar goBack={this.close} title={strings.addLost} />
          <div className={classes.content}>
            <Container>
              <div className={classes.containerInner}>
                <AddForm onChange={this.onChangeValue} onValid={this.onValid} valor={value} />
                <UploadInput text={imageName} onChange={this.onChangeImage} loading={loadingImage} />
                <Typography variant="caption">{strings.location}</Typography>
                <Map />
                <Button variant="contained" color="secondary" className={classes.button} onClick={this.save}>
                  {strings.save}
                </Button>
              </div>
            </Container>
          </div>
        </div>
      </Dialog>
    )
  }
}
