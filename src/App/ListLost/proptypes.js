import PropTypes from 'prop-types'

export const propTypes = {
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // router
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}
