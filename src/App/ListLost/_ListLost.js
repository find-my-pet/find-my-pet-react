import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight'
import AddIcon from '@material-ui/icons/Add'
import CircularProgress from '@material-ui/core/CircularProgress'

import { updateUrlState } from 'local-modules/url-state'

import { MenuBottom } from 'common/MenuBottom'
import { AppBar } from 'common/AppBar'
import { Container } from 'common/Container'
import { DialogConfirmDeactivate } from 'common/DialogConfirmDeactivate'

import { lostCollection } from 'api/collections'

import { CardLost } from './CardLost'
import { FilterLost } from './FilterLost'
import { AddLost } from './AddLost'
import { DialogAddLost } from './DialogAddLost'
import { DialogFilterLost } from './DialogFilterLost'

export class _ListLost extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      unsubscribeLost: null,
      loading: true,
      lostSnap: [],
      disableFloatElements: false,
    }
  }

  componentDidMount = () => {
    this.getInstancias()
  }

  componentWillReceiveProps = nextProps => {
    const { hidden } = this.props.location.state
    const hiddenNext = nextProps.location.state.hidden
    const isDialogAddActive = Boolean(hidden.isDialogAddActive)
    const isDialogAddActiveNext = Boolean(hiddenNext.isDialogAddActive)
    const isDialogFilterActive = Boolean(hidden.isDialogFilterActive)
    const isDialogFilterActiveNext = Boolean(hiddenNext.isDialogFilterActive)
    const hasDialogAddActived = isDialogAddActiveNext && !isDialogAddActive
    const hasDialogFilterActived = isDialogFilterActiveNext && !isDialogFilterActive
    if (hasDialogAddActived || hasDialogFilterActived) this.setState({ disableFloatElements: false })
  }

  componentWillUnmount = () => {
    const { unsubscribeLost } = this.state
    if (unsubscribeLost) unsubscribeLost()
  }

  getInstancias = () => {
    this.setState({
      unsubscribeLost: lostCollection.where('active', '==', true).onSnapshot(this.setLost),
    })
  }

  setLost = lostSnap => {
    this.setState({ lostSnap, loading: false })
  }

  handleOpenDialogAdd = () => {
    this.setState({ disableFloatElements: true }, this.openDialogAdd)
  }

  openDialogAdd = () => {
    updateUrlState({ hidden: { isDialogAddActive: true } })
  }

  handleOpenDialogFilter = () => {
    this.setState({ disableFloatElements: true }, this.openDialogFilter)
  }

  openDialogFilter = () => {
    updateUrlState({ hidden: { isDialogFilterActive: true } })
  }

  render() {
    const { classes, strings } = this.props
    const { lostSnap, loading, disableFloatElements } = this.state
    const nothingFound = lostSnap.size === 0
    const instances = []
    lostSnap.forEach(instanceRef => instances.push({ ...instanceRef.data(), id: instanceRef.id }))
    return (
      <div className={classes.root}>
        <AppBar title={strings.lost} />
        <div className={classes.content}>
          <Container>
            {loading && (
              <div className={classes.loadingContainer}>
                <CircularProgress />
              </div>
            )}
            {!loading && (
              <div className={classes.containerInner}>
                {instances.map((instance, index) => (
                  <CardLost key={index} instance={instance} />
                ))}
                {nothingFound && <Paper className={classes.nothingFound}>{strings.nothingFoundLost}</Paper>}
                <Paper className={classes.allLoaded}>
                  <KeyboardArrowRightIcon className={classes.icon} />
                  <Typography>{strings.lostMessage}</Typography>
                </Paper>
                {!disableFloatElements && (
                  <Button color="secondary" className={classes.fab} variant="fab" onClick={this.handleOpenDialogAdd}>
                    <AddIcon />
                  </Button>
                )}
              </div>
            )}
          </Container>
        </div>
        {!disableFloatElements && (
          <MenuBottom composition={['searchWithDialog', 'donation']} handleOpenFilter={this.handleOpenDialogFilter} />
        )}
        <DialogConfirmDeactivate text={strings.confirmDeactivate} collection={lostCollection} />
        <FilterLost />
        <AddLost />
        <DialogAddLost />
        <DialogFilterLost />
      </div>
    )
  }
}
