export const style = {
  item: {
    whiteSpace: 'normal',
    wordWrap: 'break-word',
    height: 'auto',
  },
}
