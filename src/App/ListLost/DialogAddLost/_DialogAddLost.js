import React, { Component } from 'react'
import { propTypes } from './proptypes'

import { Dialog, Slide, Paper, MenuList, MenuItem } from '@material-ui/core'

import { updateUrlState } from 'local-modules/url-state'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export class _DialogAddLost extends Component {
  static propTypes = propTypes

  close = () => {
    updateUrlState({ hidden: { isDialogAddActive: false } })
  }

  openAdd = () => {
    updateUrlState({ hidden: { isAddActive: true, isDialogAddActive: false } })
  }

  render() {
    const { classes, strings, history } = this.props
    const { isDialogAddActive } = history.location.state.hidden
    return (
      <Dialog open={Boolean(isDialogAddActive)} onClose={this.close} TransitionComponent={Transition}>
        <Paper>
          <MenuList>
            <MenuItem className={classes.item} onClick={this.openAdd}>
              {strings.dialogLostAdd}
            </MenuItem>
            <MenuItem className={classes.item} onClick={this.openAdd}>
              {strings.dialogFoundAdd}
            </MenuItem>
          </MenuList>
        </Paper>
      </Dialog>
    )
  }
}
