import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import Slide from '@material-ui/core/Slide'
import Typography from '@material-ui/core/Typography'

import { SimpleForm, CampoText, CampoSlider } from 'local-modules/simple-form'
import { updateUrlState } from 'local-modules/url-state'

import { AppBar } from 'common/AppBar'
import { Container } from 'common/Container'
import { googleKey } from 'api/googleKey'

function Transition(props) {
  return <Slide direction="up" {...props} />
}

export class _FilterLost extends Component {
  static propTypes = propTypes

  state = {
    value: {
      contactInformation: '',
      description: '',
    },
    isValid: false,
    valueMock: {
      locationRange: 5,
    },
    isValidMock: false,
  }

  onValidMock = isValidMock => this.setState({ isValidMock })

  onChangeValueMock = valueMock => this.setState({ valueMock })

  getSliderCaption = numero => {
    return `${numero} km`
  }

  onValid = isValid => this.setState({ isValid })

  onChangeValue = value => this.setState({ value })

  closeFilter = () => {
    updateUrlState({ hidden: { isFilterActive: false } })
  }

  search = () => {
    this.closeFilter()
  }

  render() {
    const { classes, strings, history } = this.props
    const { value, valueMock } = this.state
    const { isFilterActive } = history.location.state.hidden
    return (
      <Dialog fullScreen open={Boolean(isFilterActive)} onClose={this.handleClose} TransitionComponent={Transition}>
        <div className={classes.root}>
          <AppBar goBack={this.closeFilter} title={strings.search} />
          <div className={classes.content}>
            <Container>
              <div className={classes.containerInner}>
                <SimpleForm onChange={this.onChangeValueMock} onValid={this.onValidMock} valor={valueMock}>
                  <CampoSlider
                    sliderProps={{
                      caption: this.getSliderCaption,
                      label: strings.locationRange,
                      min: 1,
                      max: 100,
                      width: '200px',
                    }}
                    accessor="locationRange"
                  />
                </SimpleForm>
                <Typography variant="caption">{strings.location}</Typography>
                <iframe
                  className={classes.map}
                  title="map"
                  width="100%"
                  height="450"
                  frameBorder="0"
                  src={`https://www.google.com/maps/embed/v1/place?key=${googleKey}
    &q=Space+Needle,Seattle+WA`}
                  allowFullScreen
                />
                <SimpleForm onChange={this.onChangeValue} onValid={this.onValid} valor={value}>
                  <CampoText
                    accessor="description"
                    required
                    textFieldProps={{
                      label: strings.description,
                      fullWidth: true,
                      multiline: true,
                      rows: 2,
                      rowsMax: 10,
                      variant: 'outlined',
                    }}
                  />
                  <CampoText
                    accessor="contactInformation"
                    required
                    textFieldProps={{
                      label: strings.contactInformation,
                      fullWidth: true,
                      multiline: true,
                      rows: 2,
                      rowsMax: 10,
                      variant: 'outlined',
                    }}
                  />
                </SimpleForm>
                <Button variant="contained" color="secondary" className={classes.button} onClick={this.search}>
                  {strings.search}
                </Button>
              </div>
            </Container>
          </div>
        </div>
      </Dialog>
    )
  }
}
