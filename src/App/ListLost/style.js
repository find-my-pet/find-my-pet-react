const containerInner = {
  display: 'flex',
  flexWrap: 'wrap',
  padding: '0 0 10px 0',
}

export const style = theme => ({
  root: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  containerInner,
  content: {
    flexGrow: 1,
  },
  fab: {
    position: 'sticky',
    bottom: '60px',
    left: '99999999px',
    marginRight: 10,
  },
  nothingFound: {
    display: 'flex',
    alignItems: 'center',
    width: '100%',
    padding: '5px 10px',
    margin: '10px',
  },
  allLoaded: {
    display: 'flex',
    alignItems: 'center',
    width: 'calc(100% - 110px)',
    padding: '5px 10px',
    margin: '0 0 0 10px',
  },
  icon: {
    marginRight: 5,
  },
  [theme.breakpoints.up('md')]: {
    containerInner: {
      ...containerInner,
      margin: '10px 0 0 0',
    },
  },
  loadingContainer: {
    width: '100%',
    flexGrow: 1,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    margin: '10px',
  },
})
