import { bindActionCreators } from 'redux'

export function appMapStateToProps(state) {
  return {
    user: state.user,
    userInfo: state.userInfo,
    loadingUser: state.loadingUser,
  }
}

export function appMapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
