import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import { Display } from './Display'

import React from 'react'

Enzyme.configure({ adapter: new Adapter() })

jest.mock('common/Container', () => ({
  Container: props => <div>{props.children}</div>,
}))

jest.mock('./BarraSuperior', () => ({
  BarraSuperior: props => <div>{props.children}</div>,
}))

const defaultProps = {
  classes: {
    display: '.display',
  },
  children: <div>Conteúdo</div>,
}

describe('BotaoMenuEsquerdo unit test', () => {
  test('Monta', () => {
    const component = mount(<Display {...defaultProps} />)
    component.unmount()
  })
  test('Children é aplicado.', () => {
    const component = mount(<Display {...defaultProps} />)
    expect(component.contains(defaultProps.children)).toBeTruthy()
    component.unmount()
  })
})
