import React from 'react'
import Enzyme, { shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

Enzyme.configure({ adapter: new Adapter() })

const Ca = () => (
  <div>
    <div>a</div>
  </div>
)

it('CheckboxWithLabel changes the text after click', () => {
  const wrapper = shallow(<Ca />)
  expect(wrapper.contains('a')).toBe(true)
  // expect(check).toEqual(<div>a</div>)
})

test('the best flavor is grapefruit', () => {
  const wrapper = shallow(<Ca />)
  expect(wrapper.contains(<div>a</div>)).toBe(true)
})
