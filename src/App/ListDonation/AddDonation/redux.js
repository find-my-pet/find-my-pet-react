import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    userInfo: state.userInfo,
    user: state.user,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
