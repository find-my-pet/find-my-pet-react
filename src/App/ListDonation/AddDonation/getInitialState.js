export const getInitialState = () => ({
  value: {
    contactInformation: '',
    description: '',
  },
  image: '',
  imageName: 'Adicionar foto',
  imageUrl: '',
  isValid: false,
  loadingImage: false,
  imageId: null,
})
