import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
// import { withRouter } from 'react-router'

import { _AddDonationForm } from './_AddDonationForm'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const AddDonationForm = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(_AddDonationForm)
