import { bindActionCreators } from 'redux'

export function mapStateToProps(state) {
  return {
    userInfo: state.userInfo,
    strings: state.strings,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}
