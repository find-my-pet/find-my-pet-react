import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Button from '@material-ui/core/Button'
import Paper from '@material-ui/core/Paper'
import Typography from '@material-ui/core/Typography'

import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight'
import AddIcon from '@material-ui/icons/Add'
import CircularProgress from '@material-ui/core/CircularProgress'

import { updateUrlState } from 'local-modules/url-state'

import { MenuBottom } from 'common/MenuBottom'
import { AppBar } from 'common/AppBar'
import { Container } from 'common/Container'
import { DialogConfirmDeactivate } from 'common/DialogConfirmDeactivate'

import { adoptionCollection } from 'api/collections'

import { CardDonation } from './CardDonation'
import { FilterDonation } from './FilterDonation'
import { AddDonation } from './AddDonation'

export class _ListDonation extends Component {
  static propTypes = propTypes

  constructor(props) {
    super(props)
    this.state = {
      unsubscribe: null,
      loading: true,
      snap: [],
      disableFloatElements: false,
    }
  }

  componentDidMount = () => {
    this.getInstancias()
  }

  componentWillReceiveProps = nextProps => {
    const { hidden } = this.props.location.state
    const hiddenNext = nextProps.location.state.hidden
    const isAddActive = Boolean(hidden.isAddActive)
    const isAddActiveNext = Boolean(hiddenNext.isAddActive)
    const isFilterActive = Boolean(hidden.isFilterActive)
    const isFilterActiveNext = Boolean(hiddenNext.isFilterActive)
    const hasDialogAddActived = isAddActiveNext && !isAddActive
    const hasDialogFilterActived = isFilterActiveNext && !isFilterActive
    if (hasDialogAddActived || hasDialogFilterActived) this.setState({ disableFloatElements: false })
  }

  componentWillUnmount = () => {
    const { unsubscribe } = this.state
    if (unsubscribe) unsubscribe()
  }

  getInstancias = () => {
    this.setState({ unsubscribe: adoptionCollection.where('active', '==', true).onSnapshot(this.setLost) })
  }

  setLost = snap => {
    this.setState({ snap, loading: false })
  }

  handleOpenAdd = () => {
    this.setState({ disableFloatElements: true }, this.openAdd)
  }

  openAdd = () => {
    updateUrlState({ hidden: { isAddActive: true } })
  }

  handleOpenFilter = () => {
    this.setState({ disableFloatElements: true }, this.openFilter)
  }

  openFilter = () => {
    updateUrlState({ hidden: { isFilterActive: true } })
  }

  render() {
    const { classes, strings, userInfo } = this.props
    const { snap, loading, disableFloatElements } = this.state
    const isOrg = userInfo ? userInfo.isOrg : false
    const nothingFound = snap.size === 0
    const instances = []
    snap.forEach(instanceRef => instances.push({ ...instanceRef.data(), id: instanceRef.id }))
    return (
      <div className={classes.root}>
        <AppBar title={strings.donation} />
        <div className={classes.content}>
          <Container>
            {loading && (
              <div className={classes.loadingContainer}>
                <CircularProgress />
              </div>
            )}
            {!loading && (
              <div className={classes.containerInner}>
                {instances.map((instance, index) => (
                  <CardDonation key={index} instance={instance} />
                ))}
                {nothingFound && <Paper className={classes.nothingFound}>{strings.nothingFoundDonation}</Paper>}
                {isOrg && (
                  <Paper className={classes.allLoaded}>
                    <KeyboardArrowRightIcon className={classes.icon} />
                    <Typography>{strings.donationMessage}</Typography>
                  </Paper>
                )}
                {isOrg && !disableFloatElements && (
                  <Button color="secondary" className={classes.fab} variant="fab" onClick={this.handleOpenAdd}>
                    <AddIcon />
                  </Button>
                )}
              </div>
            )}
          </Container>
        </div>
        <DialogConfirmDeactivate text={strings.confirmDeactivate} collection={adoptionCollection} />
        {!disableFloatElements && (
          <MenuBottom composition={['lost', 'search']} handleOpenFilter={this.handleOpenFilter} />
        )}
        <FilterDonation />
        <AddDonation />
      </div>
    )
  }
}
