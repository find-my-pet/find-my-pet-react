import PropTypes from 'prop-types'

export const propTypes = {
  // redux state
  userInfo: PropTypes.object,
  // strings
  strings: PropTypes.object.isRequired,
  // style
  classes: PropTypes.object.isRequired,
  // router
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
}
