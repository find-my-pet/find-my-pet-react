import PropTypes from 'prop-types'

const lostInstance = PropTypes.shape({
  description: PropTypes.string.isRequired,
  contactInformation: PropTypes.string.isRequired,
  imageUrl: PropTypes.string.isRequired,
}).isRequired

export const propTypes = {
  instance: lostInstance,
  // style
  classes: PropTypes.object.isRequired,
  // strings
  strings: PropTypes.object.isRequired,
  // router
  // history: PropTypes.object.isRequired,
  // location: PropTypes.object.isRequired,
}
