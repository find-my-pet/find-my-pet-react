import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'

import ShareIcon from '@material-ui/icons/Share'
import SpeakerNotesIcon from '@material-ui/icons/SpeakerNotes'
import DoneIcon from '@material-ui/icons/Done'

import { updateUrlState } from 'local-modules/url-state'

export class _CardDonation extends Component {
  static propTypes = propTypes

  openDeactivateDialog = () => {
    const { instance } = this.props
    updateUrlState({ hidden: { isDialogConfirmDeactivateActive: true, instanceId: instance.id } })
  }

  render() {
    const { instance, strings, classes, user } = this.props
    const { imageUrl, contactInformation, description, orgName, userId } = instance
    const userIsOwner = user && user.uid === userId
    return (
      <div className={classes.root}>
        <img className={classes.image} src={imageUrl} alt="animal" />
        <div className={classes.content}>
          <div className={classes.contentBlock}>
            <Typography variant="caption">{strings.reponsableOrg}</Typography>
            <Typography>{orgName}</Typography>
          </div>
          <div className={classes.contentBlock}>
            <Typography variant="caption">{strings.description}</Typography>
            <Typography className={classes.text}>{description}</Typography>
          </div>
          <div>
            <Typography variant="caption">{strings.contactInformation}</Typography>
            <Typography className={classes.text}>{contactInformation}</Typography>
          </div>
          <div className={classes.actions}>
            <IconButton color="primary">
              <SpeakerNotesIcon />
            </IconButton>
            <IconButton>
              <ShareIcon />
            </IconButton>
            {userIsOwner && (
              <IconButton>
                <DoneIcon onClick={this.openDeactivateDialog} />
              </IconButton>
            )}
          </div>
        </div>
      </div>
    )
  }
}
