export const style = theme => ({
  root: {
    width: '100%',
  },
  content: {
    padding: '10px',
  },
  imageContainer: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: theme.palette.common.black,
  },
  image: {
    width: '100%',
    objectFit: 'contain',
    boxShadow: theme.elevation,
  },
  contentBlock: {
    margin: '0 0 10px 0',
  },
  text: {
    whiteSpace: 'pre-wrap',
  },
})
