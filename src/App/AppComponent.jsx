import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Switch, Route } from 'react-router-dom'

import { setUrlState } from 'local-modules/url-state'

import { Dialog } from 'common/Dialog'
import { listenUserChange } from 'api/onAuthStateChanged'

import { PaginaNaoEncontrada } from './PaginaNaoEncontrada'
import { ListLost } from './ListLost'
import { ListDonation } from './ListDonation'
import { SignUpForm } from './SignUpForm'
import { Snackbar } from './Snackbar'

export class AppComponent extends Component {
  static propTypes = {
    // redux state
    user: PropTypes.object,
    loadingUser: PropTypes.bool,
    // style
    classes: PropTypes.object.isRequired,
    // router
    history: PropTypes.object.isRequired,
  }

  static defaultProps = {
    defaultUrlState: {
      hidden: {},
      string: {},
    },
  }

  state = {
    unsubscribeUserChange: listenUserChange(),
  }

  isValidUrlState

  constructor(props) {
    super(props)
    this.validadeUrlState(props)
  }

  componentWillReceiveProps = next => {
    this.validadeUrlState(next)
  }

  componentWillUnmount = () => this.state.unsubscribeUserChange()

  validadeUrlState = props => {
    const { location, defaultUrlState } = props
    const isValidUrlState = this.checkUrlState(location.state)
    if (isValidUrlState) this.isValidUrlState = true
    else {
      this.isValidUrlState = false
      setUrlState(defaultUrlState)
    }
  }

  checkUrlState = state => {
    if (!state) return false
    if (!state.hidden) return false
    if (!state.string) return false
    return true
  }

  render() {
    const { classes, loadingUser } = this.props
    if (!loadingUser && this.isValidUrlState)
      return (
        <div className={classes.app}>
          <Switch>
            <Route exact path="/" component={ListLost} />
            <Route exact path="/donation" component={ListDonation} />
            <Route path="/" component={PaginaNaoEncontrada} />
          </Switch>
          <Dialog />
          <SignUpForm />
          <Snackbar />
        </div>
      )
    return <div />
  }
}
