import React, { Component } from 'react'
import { propTypes } from './proptypes'

import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'

export class _Snackbar extends Component {
  static propTypes = propTypes

  state = {
    open: false,
    message: '',
  }

  componentWillReceiveProps = next => {
    const { snackbarMessages } = this.props
    if (snackbarMessages !== next.snackbarMessages) this.processQueue(next)
  }

  processQueue = props => {
    const { snackbarMessages, setSnackbarMessages } = props
    const { open } = this.state
    if (snackbarMessages.length === 0 || open) return
    const message = snackbarMessages.shift()
    this.setState({ message, open: true })
    setSnackbarMessages(snackbarMessages)
    return
  }

  handleClick = () => {
    this.setState({ open: true })
  }

  handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }
    this.setState({ open: false }, () => this.processQueue(this.props))
  }

  render() {
    const { classes } = this.props
    const { open, message } = this.state
    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        open={open}
        autoHideDuration={6000}
        onClose={this.handleClose}
        ContentProps={{
          'aria-describedby': 'message-id',
        }}
        message={<span id="message-id">{message}</span>}
        action={[
          <IconButton
            key="close"
            aria-label="Close"
            color="inherit"
            className={classes.close}
            onClick={this.handleClose}
          >
            <CloseIcon />
          </IconButton>,
        ]}
      />
    )
  }
}
