import { bindActionCreators } from 'redux'
import { actions } from 'utils/actions'

export function mapStateToProps(state) {
  return {
    strings: state.strings,
    snackbarMessages: state.snackbarMessages,
  }
}

export function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      setSnackbarMessages: actions.select.snackbarMessages,
    },
    dispatch,
  )
}
