import { auth } from './auth'
import { setUserInStore } from './setUserInStore'
import { fetchUserInfo } from './fetchUserInfo'

export const listenUserChange = () =>
  auth.onAuthStateChanged(user => {
    if (user) fetchUserInfo(user).then(setUserInStore(user))
    else setUserInStore(user)()
  })
