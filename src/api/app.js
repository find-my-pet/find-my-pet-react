import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
import 'firebase/storage'

var config = {
  apiKey: 'AIzaSyAe5fty7IyDIxH5RN-aZl1m-aOniWO4gaU',
  authDomain: 'find-my-pet-dd834.firebaseapp.com',
  databaseURL: 'https://find-my-pet-dd834.firebaseio.com',
  projectId: 'find-my-pet-dd834',
  storageBucket: 'gs://find-my-pet-dd834.appspot.com',
  messagingSenderId: '374240734008',
}

export const app = firebase.initializeApp(config)
