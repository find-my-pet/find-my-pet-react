import { storageRef } from 'api/storage'

export const getDownloadLink = id => {
  return storageRef.child(id).getDownloadURL()
}
