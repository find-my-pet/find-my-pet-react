import { dispatchToStore } from 'utils/compositeActions'

export const setUserInStore = user => userInfoSnap => {
  if (userInfoSnap) dispatchToStore('select', 'userInfo', userInfoSnap.data())
  else dispatchToStore('select', 'userInfo', null)
  dispatchToStore('select', 'user', user)
  dispatchToStore('select', 'loadingUser', false)
}
