import { app } from './app'

export const db = app.firestore()

const settings = { timestampsInSnapshots: true }
db.settings(settings)
