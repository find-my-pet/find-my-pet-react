import { app } from './app'

export const storage = app.storage()

export const storageRef = storage.ref()
