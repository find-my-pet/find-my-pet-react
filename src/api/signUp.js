import { auth } from './auth'
import { setUserInfo } from './setUserInfo'

export const signUp = (email, password, userInfo) =>
  auth.createUserWithEmailAndPassword(email, password).then(setUserInfo(userInfo))
