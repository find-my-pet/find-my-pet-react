import { userInfoCollection } from './collections'

export const fetchUserInfo = user => userInfoCollection.doc(user.uid).get()
