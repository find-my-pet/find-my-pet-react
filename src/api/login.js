import { auth } from './auth'

export const login = (email, password) => auth.signInWithEmailAndPassword(email, password)
