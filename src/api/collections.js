import { db } from './db'

export const userInfoCollection = db.collection('userInfo')
export const orgCollections = db.collection('org')
export const foundCollections = db.collection('found')
export const lostCollection = db.collection('lost')
export const adoptionCollection = db.collection('adoption')
