import { userInfoCollection } from './collections'

export const setUserInfo = userInfo => userCredentials => {
  return userInfoCollection.doc(userCredentials.user.uid).set(userInfo)
}
