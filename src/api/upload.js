import { storageRef } from 'api/storage'

export const upload = (name, file) => {
  return storageRef.child(name).put(file)
}
