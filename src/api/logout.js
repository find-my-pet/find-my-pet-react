import { auth } from './auth'

export const logout = () => auth.signOut()
