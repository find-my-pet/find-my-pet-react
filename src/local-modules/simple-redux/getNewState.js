import { operations } from './operations'

export const getNewState = (currentState, action, stateKey) => {
  const operation = operations[action.type]
  if (action.stateKey === stateKey) {
    if (operation.usesData && operation.usesCurrentState) return operation.getNewState(currentState, action.data)
    else if (operation.usesData) return operation.getNewState(action.data)
    else if (operation.usesCurrentState) return operation.getNewState(currentState)
  }
  return currentState
}
