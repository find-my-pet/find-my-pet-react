export const withData = (operationKey, stateKey) => {
  return data => {
    return {
      type: operationKey,
      stateKey,
      data,
    }
  }
}
