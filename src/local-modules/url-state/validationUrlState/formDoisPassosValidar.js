export const formDoisPassosValidar = ({ hidden }) => {
  let validation = {
    isValid: true,
    defaultState: {
      hidden: {
        activeStep: 0,
        isOpenModal: false,
        idsCardsSelecionados: [],
      },
      string: {},
    },
  }
  const { isOpenModal, activeStep, idsCardsSelecionados } = hidden
  if (typeof isOpenModal !== 'boolean') validation.isValid = false
  // if (valorCaderno) return validation
  if (activeStep < 0 || activeStep > 1) validation.isValid = false
  if (!Array.isArray(idsCardsSelecionados)) validation.isValid = false
  return validation
}
