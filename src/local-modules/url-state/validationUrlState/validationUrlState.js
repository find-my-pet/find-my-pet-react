import { getEmptyUrlState } from './getEmptyUrlState'
import { formDoisPassosValidar } from './formDoisPassosValidar'

export const validationUrlState = location => {
  let { state, pathname } = location
  let validation = {
    isValid: true,
    defaultState: getEmptyUrlState(),
  }
  if (!state || !state.hidden || !state.string) {
    validation.isValid = false
    state = validation.defaultState
  }
  const rootPath = pathname.split('/')[1]
  switch (rootPath) {
    case 'caderno':
      validation = formDoisPassosValidar(state)
      break
    case 'concurso':
      validation = formDoisPassosValidar(state)
      break
    case 'prova':
      validation = formDoisPassosValidar(state)
      break
    default:
      console.warn('Rota não tem validação de urlState.')
  }
  return validation
}
