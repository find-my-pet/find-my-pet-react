import qs from 'qs'

import { history } from 'utils/history'

export const setUrlState = ({ pathname, hidden, string }) => {
  const newUrl = {
    pathname: pathname ? pathname : history.location.pathname,
    state: {
      hidden: hidden,
      string: string,
    },
  }
  if (hidden === null || hidden === undefined) newUrl.state.hidden = {}
  if (string === null || string === undefined) newUrl.state.string = {}
  else newUrl.search = qs.stringify(newUrl.state.string)
  history.push(newUrl)
}
