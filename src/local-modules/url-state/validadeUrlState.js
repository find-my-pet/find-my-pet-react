import { validationUrlState } from './validationUrlState'
import { replaceState } from './replaceState'

export const validadeUrlState = history => {
  const { isValid, defaultState } = validationUrlState(history.location)
  if (!isValid) {
    console.warn('Sobreescrevendo urlState com padrão.')
    replaceState(defaultState)
  }
  return isValid
}
