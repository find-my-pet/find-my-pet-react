import qs from 'qs'

import { history } from 'utils/history'

export const replaceState = ({ pathname, hidden, string }) => {
  const newUrl = {
    state: {
      hidden: hidden,
      string: string,
    },
  }
  if (hidden === null || hidden === undefined) newUrl.state.hidden = {}
  if (string === null || string === undefined) newUrl.state.string = {}
  else newUrl.search = qs.stringify(newUrl.state.string)
  history.replace(newUrl)
}
