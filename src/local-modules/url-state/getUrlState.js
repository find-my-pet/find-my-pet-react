import { history } from 'utils/history'

export const getUrlState = () => {
  const { state, pathname } = history.location
  return { pathname, hidden: state.hidden, string: state.string }
}
