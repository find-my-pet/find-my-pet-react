export const padronizarPropsBool = props => {
  const campoProps = { ...props }
  campoProps.campoEspecilizadoProps = campoProps.boolProps ? campoProps.boolProps : {}
  delete campoProps.boolProps
  if (campoProps.label) {
    campoProps.campoEspecilizadoProps.label = campoProps.label
    delete campoProps.label
  }
  return campoProps
}
