export const padronizarPropsSlider = props => {
  const campoProps = { ...props }
  campoProps.campoEspecilizadoProps = props.sliderProps
  delete campoProps.sliderProps
  return campoProps
}
