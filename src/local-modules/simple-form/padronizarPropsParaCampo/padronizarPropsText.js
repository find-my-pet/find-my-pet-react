export const padronizarPropsText = props => {
  const campoProps = { ...props }
  campoProps.campoEspecilizadoProps = props.textFieldProps
  delete campoProps.textFieldProps
  return campoProps
}
