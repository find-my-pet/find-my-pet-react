import { padronizarPropsText } from './padronizarPropsText'
import { padronizarPropsBool } from './padronizarPropsBool'
import { padronizarPropsSlider } from './padronizarPropsSlider'
import { padronizarPropsSelect } from './padronizarPropsSelect'

export const padronizarProps = (campoProps, valorDoGrupo, onCampoChange, onCampoValid, adicionarRefEmCamposRefs) => {
  let props = { ...campoProps }
  const { accessor } = campoProps
  props = {
    ...campoProps,
    valor: valorDoGrupo[accessor],
    valorPai: valorDoGrupo,
    onChange: onCampoChange(accessor),
    onValid: onCampoValid(accessor),
    key: accessor,
    onRef: adicionarRefEmCamposRefs(accessor),
  }
  switch (campoProps.tipo) {
    case 'text':
      return padronizarPropsText(props)
    case 'bool':
      return padronizarPropsBool(props)
    case 'slider':
      return padronizarPropsSlider(props)
    case 'select':
      return padronizarPropsSelect(props)
    case 'objeto':
      return props
    default:
      console.error('Nome do campo especificado não existe: ' + props.tipo)
  }
}
