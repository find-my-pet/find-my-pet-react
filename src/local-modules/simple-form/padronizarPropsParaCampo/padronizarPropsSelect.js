export const padronizarPropsSelect = props => {
  const campoProps = { ...props }
  campoProps.campoEspecilizadoProps = props.selectProps
  campoProps.campoEspecilizadoProps.children = props.children
  campoProps.campoEspecilizadoProps.helperText = props.helperText
  delete campoProps.selectProps
  delete campoProps.children
  delete campoProps.helperText
  return campoProps
}
