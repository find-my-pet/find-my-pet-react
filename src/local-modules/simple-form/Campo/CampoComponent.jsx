import React, { Component } from 'react'

import { store } from 'utils/store'

import { InputText } from './InputText'
import { InputBool } from './InputBool'
import { InputSlider } from './InputSlider'
import { CampoWrapperPadrao } from './CampoWrapperPadrao'
import { validarCampo } from './validarCampo'
import { campoPropTypes } from './campoPropTypes'
import { InputSelect } from './InputSelect'

export class CampoComponent extends Component {
  static propTypes = campoPropTypes

  constructor(props) {
    super(props)
    const { required, urlValidacao } = props
    this.state = {
      isValidPadrao: false,
      isValidUrl: !(urlValidacao && required),
      erros: [],
      exibirErro: false,
      erroText: null,
      carregando: false,
    }
  }

  mostrarErros = () => {
    const { _imprimirErros } = this
    const { erros } = this.state
    if (erros.length) {
      _imprimirErros(erros)
    }
  }

  onBlur = () => {
    const { _imprimirErros, _validarUrl } = this
    const { urlValidacao, valor } = this.props
    const { isValidPadrao, erros } = this.state
    if (urlValidacao) {
      _validarUrl(valor)
    } else {
      if (!isValidPadrao) _imprimirErros(erros)
    }
  }

  onFocus = () => {
    const { onFocus } = this.props
    const { exibirErro } = this.state
    if (exibirErro) this.setState({ exibirErro: false, erroText: '' })
    if (onFocus) onFocus()
  }

  onChange = novoValor => {
    const { accessor, onChange } = this.props
    const { exibirErro } = this.state
    if (exibirErro) this.setState({ exibirErro: false, erroText: '' })
    onChange(novoValor, accessor)
  }

  componentDidMount = () => {
    const { _validar } = this
    const { valor, valorPai, onRef } = this.props
    if (onRef) onRef(this)
    _validar(valor, valorPai)
  }

  componentWillReceiveProps = proximoProps => {
    const { _validar } = this
    const { valor, valorPai, validarComContexto, urlValidacao } = this.props
    const novoValor = proximoProps.valor
    const novoValorPai = proximoProps.valorPai
    const temNovoValor = valor !== novoValor
    const deveValidarComContexto = valorPai !== proximoProps.valorPai && validarComContexto && !temNovoValor
    if (temNovoValor || deveValidarComContexto) {
      if (urlValidacao) this.setState({ isValidUrl: false }, () => _validar(novoValor, novoValorPai))
      else _validar(novoValor, novoValorPai)
    }
  }

  componentWillUnmount = () => {
    const { onRef } = this.props
    if (onRef) onRef(null)
  }

  _validar = (valor, valorPai) => {
    let { emptyValue, mensagensDeErro, required, validar, accessor, onValid } = this.props
    const { isValidUrl } = this.state
    if (!mensagensDeErro) mensagensDeErro = {}
    const validacao = validarCampo({ emptyValue, required, valor, valorPai, mensagensDeErro, validarPorProp: validar })
    const isValid = isValidUrl && validacao.sucesso
    onValid(isValid, accessor)
    this.setState({ isValidPadrao: validacao.sucesso, erros: validacao.erros })
  }

  _validarUrl = valor => {
    let { emptyValue, required } = this.props
    if (emptyValue === valor && !required) return this.validarUrlCampoVazio()
    this.setState({ carregando: true })
    return
  }

  validarUrlCampoVazio = () => {
    let { accessor, onValid } = this.props
    let { isValidPadrao } = this.state
    onValid(isValidPadrao, accessor)
    this.setState({ isValid: isValidPadrao, isValidUrl: true })
  }

  _tratarRespostaDaUrlValidacao = r => {
    const { existe } = r.data
    const { _imprimirErros } = this
    let { mensagensDeErro, accessor, onValid } = this.props
    let { erros, isValidPadrao } = this.state
    if (!mensagensDeErro) mensagensDeErro = {}
    const strings = store.getState().strings
    const validacaoUrl = { sucesso: true, erros: [] }
    if (existe) {
      validacaoUrl.sucesso = false
      validacaoUrl.erros.push({
        code: 'ja_existe',
        message: mensagensDeErro['ja_existe'] || strings.jaExiste,
      })
    }
    const isValid = isValidPadrao && validacaoUrl.sucesso
    erros = [...validacaoUrl.erros, ...erros]
    onValid(isValid, accessor)
    this.setState({ isValid, erros, carregando: false, isValidUrl: validacaoUrl.sucesso })
    if (!isValid) _imprimirErros(erros)
  }

  _imprimirErros = erros => {
    let erroText
    if (erros[0]) erroText = erros[0].message
    else erroText = ''
    this.setState({ exibirErro: true, erroText })
  }

  _getInputEspecializado = () => {
    let { state, props, onBlur, onFocus, onChange } = this
    const { tipo, campoEspecilizadoProps, required, valor, urlValidacao, emptyValue } = props
    const estaVazio = emptyValue === valor
    const inputProps = {
      ...state,
      onBlur,
      onFocus,
      onChange,
      required,
      valor,
      campoEspecilizadoProps,
      fazValidacaoAssincrona: Boolean(urlValidacao),
      estaVazio,
    }
    switch (tipo) {
      case 'text':
        return <InputText {...inputProps} />
      case 'bool':
        return <InputBool {...inputProps} />
      case 'slider':
        return <InputSlider {...inputProps} />
      case 'select':
        return <InputSelect {...inputProps} />
      default:
        console.error('Não foi encontrado o tipo de campo especificado: ' + tipo)
    }
  }

  render() {
    let { CampoWrapper, index } = this.props
    if (!CampoWrapper) CampoWrapper = CampoWrapperPadrao
    return <CampoWrapper index={index}>{this._getInputEspecializado()}</CampoWrapper>
  }
}
