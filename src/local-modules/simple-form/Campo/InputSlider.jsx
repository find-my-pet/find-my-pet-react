import React from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import Slider from '@material-ui/lab/Slider'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Typography from '@material-ui/core/Typography'

const sliderStyle = theme => ({
  sliderContainer: {
    display: 'flex',
    alignItems: 'center',
  },
  slider: {
    margin: '0 15px 0 5px',
  },
})

const InputSliderComponent = props => {
  const { valor, onChange, campoEspecilizadoProps, classes, erros, exibirErro } = props
  const { caption, width, label, max, min, step } = campoEspecilizadoProps
  const sliderProps = {
    max,
    min,
    step,
    value: valor,
    onChange: (e, novoValor) => {
      if (valor !== novoValor) onChange(novoValor)
    },
  }
  const captionValue = caption ? caption(valor) : null
  return (
    <FormControl>
      {label && <Typography variant="caption">{label}</Typography>}
      <div className={classes.sliderContainer}>
        <div className={classes.slider} style={width ? { width } : null}>
          <Slider {...sliderProps} />
        </div>
        {captionValue && <Typography variant="caption">{captionValue}</Typography>}
      </div>
      {exibirErro && erros[0] && <FormHelperText error>{erros[0]}</FormHelperText>}
    </FormControl>
  )
}

InputSliderComponent.propTypes = {
  valor: PropTypes.number,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  erros: PropTypes.array.isRequired,
  erroText: PropTypes.string,
  isValidPadrao: PropTypes.bool,
  isValidUrl: PropTypes.bool,
  exibirErro: PropTypes.bool,
  required: PropTypes.bool,
  carregando: PropTypes.bool,
  estaVazio: PropTypes.bool,
  fazValidacaoAssincrona: PropTypes.bool,
  campoEspecilizadoProps: PropTypes.object,
}

export const InputSlider = withStyles(sliderStyle)(InputSliderComponent)
