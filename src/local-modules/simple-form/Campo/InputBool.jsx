import React from 'react'
import PropTypes from 'prop-types'

import Switch from '@material-ui/core/Switch'
import Checkbox from '@material-ui/core/Checkbox'
import FormControlLabel from '@material-ui/core/FormControlLabel'

import { getPropriedadesEssenciaisInputBool } from './getPropriedadesEssenciaisInputBool'

export const InputBool = props => {
  const { valor, onChange, campoEspecilizadoProps } = props
  const { tipo, label } = getPropriedadesEssenciaisInputBool(campoEspecilizadoProps)
  const boolProps = {
    ...campoEspecilizadoProps,
    checked: valor,
    onChange: (e, checked) => onChange(checked),
  }
  return (
    <FormControlLabel
      control={tipo === 'switch' ? <Switch {...boolProps} /> : <Checkbox {...boolProps} />}
      label={label}
    />
  )
}

InputBool.propTypes = {
  valor: PropTypes.bool,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  erros: PropTypes.array.isRequired,
  erroText: PropTypes.string,
  isValidPadrao: PropTypes.bool,
  isValidUrl: PropTypes.bool,
  exibirErro: PropTypes.bool,
  required: PropTypes.bool,
  carregando: PropTypes.bool,
  estaVazio: PropTypes.bool,
  fazValidacaoAssincrona: PropTypes.bool,
  campoEspecilizadoProps: PropTypes.object,
}
