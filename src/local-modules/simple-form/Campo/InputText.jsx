import React from 'react'
import PropTypes from 'prop-types'

import TextField from '@material-ui/core/TextField'

import { ValidarUrlAdornment } from './ValidarUrlAdornment'

export const InputText = props => {
  const {
    valor,
    onChange,
    onFocus,
    onBlur,
    erroText,
    isValidPadrao,
    isValidUrl,
    exibirErro,
    required,
    carregando,
    estaVazio,
    fazValidacaoAssincrona,
    campoEspecilizadoProps,
  } = props
  let inputAdornment
  if (fazValidacaoAssincrona) {
    inputAdornment = {
      endAdornment: (
        <ValidarUrlAdornment
          estaExibindoErro={exibirErro}
          estaCarregando={carregando}
          isValid={isValidUrl && isValidPadrao}
          estaVazio={estaVazio}
        />
      ),
    }
  }
  let { label } = campoEspecilizadoProps
  if (label && required) label += ' *'
  const textFieldProps = {
    ...campoEspecilizadoProps,
    value: valor,
    onChange: event => onChange(event.target.value),
    onFocus,
    onBlur,
    helperText: erroText,
    error: exibirErro,
    InputProps: inputAdornment || campoEspecilizadoProps.InputProps,
    label,
  }
  return <TextField {...textFieldProps} />
}

InputText.defaultProps = {
  campoEspecilizadoProps: {},
}

InputText.propTypes = {
  valor: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  erros: PropTypes.array.isRequired,
  erroText: PropTypes.string,
  isValidPadrao: PropTypes.bool,
  isValidUrl: PropTypes.bool,
  exibirErro: PropTypes.bool,
  required: PropTypes.bool,
  carregando: PropTypes.bool,
  estaVazio: PropTypes.bool,
  fazValidacaoAssincrona: PropTypes.bool,
  campoEspecilizadoProps: PropTypes.object,
}
