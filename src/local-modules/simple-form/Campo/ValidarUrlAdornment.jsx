import React from 'react'
import PropTypes from 'prop-types'

import InputAdornment from '@material-ui/core/InputAdornment'
import CircularProgress from '@material-ui/core/CircularProgress'

import DoneIcon from '@material-ui/icons/Done'
import LanguageIcon from '@material-ui/icons/Language'

export const ValidarUrlAdornment = props => {
  const { estaExibindoErro, estaCarregando, isValid, estaVazio } = props
  let iconAdornment
  if (estaCarregando) iconAdornment = <CircularProgress size={20} />
  else if (isValid && !estaVazio) iconAdornment = <DoneIcon />
  else iconAdornment = <LanguageIcon color={estaExibindoErro ? 'error' : 'primary'} />
  return <InputAdornment position="end">{iconAdornment}</InputAdornment>
}

ValidarUrlAdornment.propTypes = {
  estaExibindoErro: PropTypes.bool,
  estaCarregando: PropTypes.bool,
  isValid: PropTypes.bool,
  estaVazio: PropTypes.bool,
}
