import { store } from 'utils/store'

export const validarCampo = ({ emptyValue, required, valor, valorPai, validarPorProp, mensagensDeErro }) => {
  const strings = store.getState().strings
  const validacao = { sucesso: true, erros: [] }
  if (required && (valor === null || valor === undefined || valor === emptyValue)) {
    validacao.sucesso = false
    validacao.erros.push({
      code: 'campo_obrigatorio',
      message: mensagensDeErro['campo_obrigatorio'] || strings.campoObrigatorio,
    })
  }
  if (validarPorProp) {
    const validacaoPersonalizada = validarPorProp(valor, valorPai)
    if (!validacaoPersonalizada.sucesso) {
      validacao.sucesso = false
      validacaoPersonalizada.erros.forEach(erro => {
        if (mensagensDeErro[erro.code]) erro.message = mensagensDeErro[erro.code]
      })
      validacao.erros = [...validacao.erros, ...validacaoPersonalizada.erros]
    }
  }
  return validacao
}
