import React from 'react'
import PropTypes from 'prop-types'

import Select from '@material-ui/core/Select'
import FormControl from '@material-ui/core/FormControl'
import InputLabel from '@material-ui/core/InputLabel'
import FormHelperText from '@material-ui/core/FormHelperText'

export const InputSelect = props => {
  let { valor, onChange, onFocus, onBlur, erros, erroText, exibirErro, required, campoEspecilizadoProps } = props
  campoEspecilizadoProps = { ...campoEspecilizadoProps }
  let { label, helperText } = campoEspecilizadoProps
  if (label && required) label += ' *'
  delete campoEspecilizadoProps.helperText
  const selectProps = {
    ...campoEspecilizadoProps,
    value: valor,
    onChange: event => onChange(event.target.value),
    onFocus,
    onBlur,
    error: exibirErro,
  }
  return (
    <FormControl>
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
      {label && <InputLabel>{label}</InputLabel>}
      <Select {...selectProps} />
      {exibirErro && erros[0] && <FormHelperText error>{erroText}</FormHelperText>}
    </FormControl>
  )
}

InputSelect.defaultProps = {
  campoEspecilizadoProps: {},
}

InputSelect.propTypes = {
  valor: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
  onFocus: PropTypes.func.isRequired,
  onBlur: PropTypes.func.isRequired,
  erros: PropTypes.array.isRequired,
  erroText: PropTypes.string,
  isValidPadrao: PropTypes.bool,
  isValidUrl: PropTypes.bool,
  exibirErro: PropTypes.bool,
  required: PropTypes.bool,
  carregando: PropTypes.bool,
  estaVazio: PropTypes.bool,
  fazValidacaoAssincrona: PropTypes.bool,
  campoEspecilizadoProps: PropTypes.object,
}
