import PropTypes from 'prop-types'

export const campoPropTypes = {
  tipo: PropTypes.string.isRequired,
  accessor: PropTypes.string.isRequired,
  emptyValue: PropTypes.any,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  urlValidacao: PropTypes.string,
  validar: PropTypes.func,
  CampoWrapper: PropTypes.any,
  campoEspecilizadoProps: PropTypes.object,
  // injetado
  valor: PropTypes.any.isRequired,
  valorPai: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  onChange: PropTypes.func.isRequired,
  onValid: PropTypes.func.isRequired,
  onRef: PropTypes.func.isRequired,
  onFocus: PropTypes.func,
  index: PropTypes.number,
}
