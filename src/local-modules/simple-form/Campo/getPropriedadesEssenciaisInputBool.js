export const getPropriedadesEssenciaisInputBool = campoEspecilizadoProps => {
  let tipo, label
  if (campoEspecilizadoProps) {
    tipo = campoEspecilizadoProps.tipo
    label = campoEspecilizadoProps.label
  }
  return { tipo, label }
}
