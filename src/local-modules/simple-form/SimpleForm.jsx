import React from 'react'
import PropTypes from 'prop-types'
import { WrapperPadrao } from 'local-modules/simple-form/WrapperPadrao'

import { GrupoCamposObjeto } from './GrupoCamposObjeto'

export const SimpleForm = props => <GrupoCamposObjeto {...props} accessor="form" GrupoWrapper={props.FormWrapper} />

SimpleForm.defaultProps = {
  FormWrapper: WrapperPadrao({ margin: '0' }),
  valorPai: {},
}

SimpleForm.propTypes = {
  onValid: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  valor: PropTypes.object.isRequired,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  onRef: PropTypes.func,
  FormWrapper: PropTypes.any,
}
