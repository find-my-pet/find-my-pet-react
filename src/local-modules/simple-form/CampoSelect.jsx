import PropTypes from 'prop-types'
import { WrapperPadrao } from 'local-modules/simple-form/WrapperPadrao'

export const CampoSelect = () => null

CampoSelect.defaultProps = {
  tipo: 'select',
  CampoWrapper: WrapperPadrao({ margin: '0 0 5px 0' }),
  emptyValue: null,
  selectProps: {},
}

CampoSelect.propTypes = {
  // padrões
  accessor: PropTypes.string.isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  CampoWrapper: PropTypes.any,
  // específico
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  selectProps: PropTypes.object,
  helperText: PropTypes.string,
}
