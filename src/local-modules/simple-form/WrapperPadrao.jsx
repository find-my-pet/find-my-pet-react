import React from 'react'
import PropTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'

const style = campoWrapper => ({ campoWrapper })

const WrapperPadraoComponent = props => {
  const { children, classes } = props
  return <div className={classes.campoWrapper}>{children}</div>
}

WrapperPadraoComponent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
  // style
  classes: PropTypes.object.isRequired,
}

export const WrapperPadrao = campoWrapperClass => withStyles(style(campoWrapperClass))(WrapperPadraoComponent)
