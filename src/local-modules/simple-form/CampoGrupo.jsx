import PropTypes from 'prop-types'
import { WrapperPadrao } from 'local-modules/simple-form/WrapperPadrao'

export const CampoGrupo = () => null

CampoGrupo.defaultProps = {
  tipo: 'grupo',
  GrupoWrapper: WrapperPadrao({ margin: '5px 0 0 0' }),
}

CampoGrupo.propTypes = {
  accessor: PropTypes.string.isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  GrupoWrapper: PropTypes.any,
  label: PropTypes.string,
  helperText: PropTypes.string,
}
