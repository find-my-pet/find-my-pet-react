import PropTypes from 'prop-types'
import { WrapperPadrao } from 'local-modules/simple-form/WrapperPadrao'

export const CampoText = () => null

CampoText.defaultProps = {
  tipo: 'text',
  CampoWrapper: WrapperPadrao({ margin: '0 0 10px 0' }),
  emptyValue: '',
}

CampoText.propTypes = {
  // padrões
  accessor: PropTypes.string.isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  CampoWrapper: PropTypes.any,
  // funcionando apenas para textField por enquanto
  urlValidacao: PropTypes.string,
  // específico
  textFieldProps: PropTypes.object,
}
