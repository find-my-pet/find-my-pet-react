import { grupoObjetoGetCampo } from './grupoObjetoGetCampo'

export const grupoObjetoGetCampos = self => () => {
  const { children } = self.props
  let campos
  if (Array.isArray(children)) {
    campos = children.map(grupoObjetoGetCampo(self))
  } else {
    campos = [grupoObjetoGetCampo(self)(children)]
  }
  return campos
}
