import React from 'react'

import { Campo } from 'local-modules/simple-form/Campo'
import { padronizarPropsParaCampo } from 'local-modules/simple-form/padronizarPropsParaCampo'

import { GrupoCamposObjeto } from '.'

export const grupoObjetoGetCampo = self => campo => {
  const { _onCampoChange, _onCampoValid, _adicionarRefEmCamposRefs } = self
  // const { valor } = self.props
  const campoProps = padronizarPropsParaCampo(
    campo.props,
    self.props.valor,
    _onCampoChange,
    _onCampoValid,
    _adicionarRefEmCamposRefs,
  )
  if (campoProps.tipo === 'objeto') return <GrupoCamposObjeto {...campoProps} />
  else return <Campo {...campoProps} />
}
