export const grupoObjetoOnCampoChange = self => accessor => valorCampo => {
  const { props } = self
  let { valor, onChange } = props
  valor = { ...valor }
  valor[accessor] = valorCampo
  onChange(valor)
  self.setState({ mostrarErros: false })
}
