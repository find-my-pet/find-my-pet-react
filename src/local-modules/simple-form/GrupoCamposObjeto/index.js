import { GrupoCamposHOC } from 'local-modules/simple-form/GrupoCamposHOC'
import { grupoCamposObjetoHOCParams } from './grupoCamposObjetoHOCParams'

export const GrupoCamposObjeto = GrupoCamposHOC(grupoCamposObjetoHOCParams)
