import { objetoCamposPropTypes } from './objetoCamposPropTypes'
import { grupoObjetoGetCampos } from './grupoObjetoGetCampos'
import { grupoObjetoOnCampoChange } from './grupoObjetoOnCampoChange'

export const grupoCamposObjetoHOCParams = {
  propTypes: objetoCamposPropTypes,
  getCampos: grupoObjetoGetCampos,
  onCampoChange: grupoObjetoOnCampoChange,
}
