import PropTypes from 'prop-types'

export const objetoCamposPropTypes = {
  // específico de objeto
  valor: PropTypes.object,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]),
}
