import PropTypes from 'prop-types'
import { WrapperPadrao } from 'local-modules/simple-form/WrapperPadrao'

export const CampoSlider = () => null

CampoSlider.defaultProps = {
  tipo: 'slider',
  CampoWrapper: WrapperPadrao({ margin: '15px 0 10px 0' }),
  emptyValue: null,
}

CampoSlider.propTypes = {
  accessor: PropTypes.string.isRequired,
  sliderProps: PropTypes.object,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  CampoWrapper: PropTypes.any,
  label: PropTypes.string,
}
