import PropTypes from 'prop-types'
import { WrapperPadrao } from 'local-modules/simple-form/WrapperPadrao'

export const CampoBool = () => null

CampoBool.defaultProps = {
  tipo: 'bool',
  CampoWrapper: WrapperPadrao({ margin: '0' }),
  emptyValue: null,
}

CampoBool.propTypes = {
  accessor: PropTypes.string.isRequired,
  boolProps: PropTypes.object,
  required: PropTypes.bool,
  urlValidacao: PropTypes.string,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  CampoWrapper: PropTypes.any,
  label: PropTypes.string,
}
