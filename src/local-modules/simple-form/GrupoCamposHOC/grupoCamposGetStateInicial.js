export const grupoCamposGetStateInicial = validar => ({
  camposComErro: new Set(),
  erros: [],
  carregando: false,
  mostrarErros: false,
  campos: [],
  camposRefs: {},
  isValidUrl: false,
  isValidPorProp: !validar,
})
