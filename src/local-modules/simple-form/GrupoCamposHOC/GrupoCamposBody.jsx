import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormLabel from '@material-ui/core/FormLabel'

import { grupoCamposBodyStyle } from './grupoCamposBodyStyle'

const GrupoCamposBodyComponent = props => {
  const { sortableElement, label, helperText, mostrarErros, erros, classes, campos } = props
  return (
    <FormControl fullWidth>
      {!sortableElement && label && <FormLabel focused>{label}</FormLabel>}
      {helperText && <FormHelperText>{helperText}</FormHelperText>}
      {mostrarErros && erros[0] && <FormHelperText error>{erros[0].message}</FormHelperText>}
      {label && <div className={classes.labelDiv} />}
      {campos}
    </FormControl>
  )
}

GrupoCamposBodyComponent.propTypes = {
  valor: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  mensagensDeErro: PropTypes.object,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  helperText: PropTypes.string,
  required: PropTypes.bool,
  index: PropTypes.number,
  sortableElement: PropTypes.bool,
  campos: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.element), PropTypes.element]),
  erros: PropTypes.array,
  mostrarErros: PropTypes.bool,
}

export const GrupoCamposBody = withStyles(grupoCamposBodyStyle)(GrupoCamposBodyComponent)
