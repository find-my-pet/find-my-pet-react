import PropTypes from 'prop-types'

export const grupoCamposPropTypes = {
  accessor: PropTypes.string.isRequired,
  onValid: PropTypes.func,
  onChange: PropTypes.func,
  valorPai: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.element]),
  helperText: PropTypes.string,
  required: PropTypes.bool,
  onRef: PropTypes.func,
  GrupoWrapper: PropTypes.any,
  index: PropTypes.number,
  sortableElement: PropTypes.bool,
  contexto: PropTypes.object,
  instancia: PropTypes.object,
  setInstanciaState: PropTypes.func,
}
