import React, { Component } from 'react'

import { grupoCamposPropTypes } from './grupoCamposPropTypes'
import { grupoCamposGetStateInicial } from './grupoCamposGetStateInicial'
import { grupoCamposDefaultProps } from './grupoCamposDefaultProps'

export const grupoCamposHOComponent = ({
  getCampos,
  propTypes,
  defaultProps,
  getStateInicial,
  onComponentDidUpdate,
  onComponentDidMount,
  onCampoChange,
}) =>
  class GrupoCamposComponent extends Component {
    static propTypes = { ...grupoCamposPropTypes, ...propTypes }

    static defaultProps = { ...grupoCamposDefaultProps, ...defaultProps }

    constructor(props) {
      super(props)
      const { validar } = props
      const stateInicialExtra = getStateInicial ? getStateInicial(props) : {}
      this.state = { ...grupoCamposGetStateInicial(validar), ...stateInicialExtra }
    }

    componentDidMount = () => {
      const { _validarGrupo, props } = this
      const { valor, valorPai, onRef } = props
      if (onRef) onRef(this)
      _validarGrupo(valor, valorPai)
      if (onComponentDidMount) onComponentDidMount(this)()
    }

    shouldComponentUpdate = (propsProx, stateProx) => {
      const { children, valor } = this.props
      const { instancias, campos } = this.state
      const childrenProx = propsProx.children
      const valorProx = propsProx.valor
      const instanciasProx = stateProx.instancias
      const camposProx = stateProx.campos
      if (valor !== valorProx) return true
      if (instancias !== instanciasProx) return true
      if (childrenProx !== children) return true
      if (campos !== camposProx) return true
      return false
    }

    componentDidUpdate = (propsAnterior, stateAnterior) => {
      const { _validarGrupo, props } = this
      const { valor } = props
      const valorAnterior = propsAnterior.valor
      const novoValorPai = propsAnterior.valorPai
      if (valor !== valorAnterior) {
        _validarGrupo(valor, novoValorPai)
      }
      if (onComponentDidUpdate) onComponentDidUpdate(this)(propsAnterior, stateAnterior)
    }

    componentWillUnmount = () => {
      const { onRef } = this.props
      if (onRef) onRef(null)
    }

    mostrarErros = () => {
      const { camposRefs } = this.state
      for (let accessor in camposRefs) {
        const campoRef = camposRefs[accessor]
        if (campoRef.mostrarErros) campoRef.mostrarErros()
      }
      this.setState({ mostrarErros: true })
    }

    setInstanciaState = index => value => {
      const { instancias } = this.state
      instancias[index].state = { ...instancias[index].state, ...value }
      this.setState({ instancias: [...instancias] })
    }

    _getCampos = getCampos(this)

    _onCampoChange = onCampoChange(this)

    _adicionarRefEmCamposRefs = accessor => ref => {
      const { camposRefs } = this.state
      if (ref) camposRefs[accessor] = ref
      else {
        this._onCampoValid(accessor)(true)
        delete camposRefs[accessor]
      }
      this.setState({ camposRefs })
    }

    _adicionarMensagemDeErro = erro => {
      let { mensagensDeErro } = this.props
      if (!mensagensDeErro) mensagensDeErro = {}
      if (mensagensDeErro[erro.code]) erro.message = mensagensDeErro[erro.code]
    }

    _validarPorProp = (novoValor, novoValorPai) => {
      let { validar } = this.props
      let validacaoPorProp = { sucesso: true, erros: [] }
      if (validar) {
        validacaoPorProp = validar(novoValor, novoValorPai)
      }
      this.setState({ isValidPorProp: validacaoPorProp.sucesso })
      return validacaoPorProp
    }

    _validarGrupo = (valor, valorPai) => {
      const { onValid } = this.props
      const { camposComErro } = this.state
      const { sucesso, erros } = this._validarPorProp(valor, valorPai)
      const isValid = camposComErro.size === 0 && sucesso
      if (Array.isArray(erros)) erros.forEach(this._adicionarMensagemDeErro)
      onValid(isValid)
      this.setState({ erros })
    }

    _onCampoValid = accessor => campoIsValid => {
      const { state, props } = this
      const { camposComErro, isValidPorProp } = state
      const { onValid } = props
      if (campoIsValid) camposComErro.delete(accessor)
      else camposComErro.add(accessor)
      const temCamposComErro = camposComErro.size > 0
      const grupoIsValid = !temCamposComErro && isValidPorProp
      if (onValid) onValid(grupoIsValid)
    }

    render() {
      const { erros, mostrarErros } = this.state
      let {
        label,
        required,
        helperText,
        GrupoWrapper,
        index,
        onChange,
        valor,
        sortableElement,
        GrupoBody,
        contexto,
        instancia,
        setInstanciaState,
      } = this.props
      if (typeof label === 'string' && required) label += ' *'
      const bodyProps = {
        label,
        required,
        helperText,
        index,
        valor,
        sortableElement,
        campos: this._getCampos(),
        erros,
        mostrarErros,
        contexto,
        instancia,
        setInstanciaState,
      }
      return (
        <GrupoWrapper index={index} onChange={onChange} valor={valor}>
          <GrupoBody {...bodyProps} />
        </GrupoWrapper>
      )
    }
  }
