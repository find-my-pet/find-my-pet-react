import PropTypes from 'prop-types'
import { WrapperPadrao } from 'local-modules/simple-form/WrapperPadrao'

export const CampoObjeto = () => null

CampoObjeto.defaultProps = {
  tipo: 'objeto',
  GrupoWrapper: WrapperPadrao({ margin: '5px 0 0 0' }),
  emptyValue: {},
}

CampoObjeto.propTypes = {
  accessor: PropTypes.string.isRequired,
  required: PropTypes.bool,
  mensagensDeErro: PropTypes.object,
  validar: PropTypes.func,
  GrupoWrapper: PropTypes.any,
  label: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.arrayOf(PropTypes.element)]).isRequired,
}
