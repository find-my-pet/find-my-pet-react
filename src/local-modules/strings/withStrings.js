import { connect } from 'react-redux'

function mapStateToProps(state) {
  return {
    strings: state.strings,
  }
}

export const withStrings = component => connect(mapStateToProps)(component)
