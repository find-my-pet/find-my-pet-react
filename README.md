# find-my-pet-react

Aplicação web (frontend) para o sistema FindMyPet.

## Instruções para rodar

Necessário instalar [Node >=v10](https://nodejs.org/en/)

1.  Clone o repositório `$ git clone git@gitlab.com:find-my-pet/find-my-pet-react.git`
2.  Entre no diretório: `$ cd find-my-pet-react`
3.  Instale as dependências: `$ npm install`
4.  Inicie o serviço através do comando `$ npm start`
