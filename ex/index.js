import { compose } from 'redux'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles'
// import { withRouter } from 'react-router'

import { _Ex } from './_Ex'
import { style } from './style'
import { mapStateToProps, mapDispatchToProps } from './redux'

export const Ex = compose(
  withStyles(style),
  // withRouter,
  connect(
    mapStateToProps,
    mapDispatchToProps,
  ),
)(_Ex)
